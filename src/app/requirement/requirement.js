angular.module('sd.requirement', ['ui.router', 'satellizer', 'sdDashboard', 'ngMessages', 'angularFileUpload', 'sdDashboard', 'ngMaterial'])
    .config(function ($stateProvider) {
        $stateProvider
            .state('requirement', {
                url: '/requirement',
                views: {
                    'main': {
                        templateUrl: 'requirement/requirement.tpl.html'

                    }
                },
                controller: 'RequirementCtrl',
                data: {
                    pageTitle: 'Требования к клиенту'
                }
            });


    })
    .controller('RequirementCtrl', function ($scope, Network, Utils, Requirement) {

        var req = {title: {ru: '', en: ''}};

        Requirement.getRequirements()
            .then(function () {
                console.log('ok');
                $scope.groups = Requirement.requirements;
            });


        $scope.addGroup = function () {
            var group = {
                title: '',
                requirements: [{title: {'ru': '', en: ''}}]

            };

            $scope.groups.unshift(group);

        };

        $scope.add = function (e, index) {
            e.stopPropagation();
            var c = Requirement.requirements[index]['requirements'];
            console.dir(c);
            if (c.length === 0 || (c[0]['title']['ru'].length !== 0 && c[0]['title']['en'].length !== 0)) {
                c.unshift({title: {ru: '', en: ''}});
            }
        };

        $scope.destroyGroup = function (e, group) {
            e.stopPropagation();


            if(group.hasOwnProperty('id')){
                Network.remove('customerrequirement', {id:group.id});

            }
            var index = _.findIndex(Requirement.requirements, group);
            Requirement.requirements.splice(index,1);
            if(group.isDefault && Requirement.requirements.length > 0){

                var i = _.findIndex(Requirement.requirements, function(item){
                    return item.id > 0;
                });

                if(i > 0){
                    Requirement.requirements[i].isDefault = true;
                    Netowrk.put('customerrequirement', Requirement.requirements[i]);
                }


            }



        };

        $scope.onKeyDown = function (evt) {
            evt.stopPropagation();

        };


        $scope.save = function (e, group, frm, index) {
            e.stopPropagation();
            var error = false;


            if (group.title.length === 0) {
                frm['title' + index].$setValidity('required', false);
                error = true;
            } else {
                frm['title' + index].$setValidity('required', true);
            }

            group.requirements.forEach(function (req, i) {
                console.dir(req);
                if (!req['title']['ru'] || req['title']['ru'].length === 0) {
                    frm['title_ru' + index + i].$setValidity('required', false);
                    error = true;


                } else {
                    frm['title_ru' + index + i].$setValidity('required', true);
                }

                if (!req['title']['en'] || req['title']['en'].length === 0 || /^[a-zA-Z0-9 ]*$/.test(req['title']['en']) === false) {
                    frm['title_en' + index + i].$setValidity('pattern', false);
                    error = true;


                } else {
                    frm['title_en' + index + i].$setValidity('pattern', true);

                }


            });

            if (error) {
                return;
            }


            if (group.hasOwnProperty('id')) {
                var o = {};
                angular.copy(group, o);
                o.owner = o.owner.id;
                o = _.pick(o, ['title', 'requirements', 'owner', 'id']);

                Network.put('customerrequirement' + '/' + o.id, group);

            } else {
                Network.post('customerrequirement', group)
                    .then(function (res) {
                        group.id = res.id;
                    });
            }


        };



        $scope.cbClick = function(evt,o){
            evt.stopPropagation();
            if(!o.id){
                return evt.preventDefault();
            }

            Utils.handleDefaults(Requirement.requirements, o, 'customerrequirement');

        };
        
        

        $scope.remove = function (group, index) {

            console.dir(group);


            var removed = group.requirements.splice(index, 1);
            console.dir(removed);

            if (removed[0].title.ru.length === 0 && removed[0].title.en.length === 0) {
                return;
            }


            if (group.hasOwnProperty('id')) {
                Network.put('customerrequirement', group, true);
            }
        };


    })
    .service('Requirement', function ($q, Network, Utils) {
        return {
            requirements: [],
            getRequirements: function () {
                var that = this;
                return $q(function (resolve) {
                    Network.get('user')
                        .then(function (user) {
                            Network.get('customerrequirement')
                                .then(function (res) {
                                    res.forEach(function (group) {

                                        group.requirements.forEach(function (c, i) {

                                            if (Utils.isJSON(c)) {
                                                group.requirements[i] = JSON.parse(c);
                                            }

                                        });

                                    });

                                    console.dir(res);

                                    that.requirements = res;


                                    return resolve(res);
                                });
                        });
                });

            }

        };
    });