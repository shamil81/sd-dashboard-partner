angular.module('utils', [])
.service('Helpers', function () {
    return {

        // Converts an array of objects to a resulting object where key is ID.
        id2Index: function (arr) {
            var result = {};
            arr.forEach(function (o) {
                result[o.id] = o;
            });
            return result;
        },


        sortCollection:function (collection, skip) {


            var o = {};

            if(!skip){
                skip = [];
            }



            _.forOwn(collection, function (arr, key) {

                var sorted = {};

                if(skip.indexOf(key) === -1){
                    arr.forEach(function (item) {


                        sorted[item.id] = item;


                    });


                }else{
                    sorted = collection[key];
                }

                o[key] = sorted;



            });


            return o;


        },




        isEmpty:function(o){
            if(o.hasOwnProperty('length')){
                return !o.length;
            }
            return !Object.keys(o).length;
        },

        closestNum:function (num, ar) {
            var closest = Number.parseInt(ar[0],10),
                diff = Math.abs(num - closest);
            ar.forEach(function (v) {
                var val = Number.parseInt(v, 10),
                newDiff = Math.abs(num - val);

                if(newDiff < diff){
                    diff = newDiff;
                    closest = val;
                }
            });

            return closest;

        }


    };
});