angular.module('sd.option', ['ui.router', 'satellizer', 'sdDashboard', 'ngMessages', 'vAccordion', 'ngMaterial'])
    .config(function ($stateProvider) {
        $stateProvider
            .state('priceoption', {
                url: '/priceoption',
                views: {
                    'main': {
                        templateUrl: 'priceoption/priceoption.tpl.html'

                    }
                },
                controller: 'OptionCtrl',
                data: {
                    pageTitle: 'Тарифные опции'
                }
            });


    })

    .controller('OptionCtrl', ['$scope', 'Option', 'Network', 'Utils', 'UserService', function ($scope, Option, Network, Utils, UserService) {



        $scope.onKeyDown = function(evt){
            evt.stopPropagation();

        };

        Option.getOptions()
            .then(function(){
                $scope.priceOptions = Option.options;
                UserService.get(function(user){

                    $scope.customOptions = Option.options.filter(function(option){
                        return option.createdBy === user.id;
                    });

                });
            });




        Option.getGroups()
            .then(function(){
                $scope.groups = Option.groups;
                console.dir($scope.groups);
            });


        $scope.addGroup = function(){
            console.log('add');

            var isDefault = _.find(Option.groups, {isDefault:true}),
                hasUnsavedGroup = _.find(Option.groups, {id:-1});

            if(hasUnsavedGroup === undefined){
                Option.groups.unshift({
                    id:-1,
                    title:'',
                    options:[],
                    isDefault:!(isDefault)
                });
            }

        };

        $scope.add = function(e,group){
            e.stopPropagation();

              group.options.unshift({priceOption:null,price:0});
        };


        $scope.destroyGroup = function(e,group){
            e.stopPropagation();

            var index = _.findIndex(Option.groups, {id:group.id}),
                i = -1;


            if(group.id > 0){
                Network.remove('priceoptiongroup', {id:group.id});
            }

            Option.groups.splice(index,1);

            if(group.isDefault && Option.groups.length > 0){

                i = _.findIndex(Option.groups, function(item){
                    return item.id > 0;
                });

                if(i > 0){
                    Option.groups[i].isDefault = true;
                    Network.put('priceoptiongroup', Option.groups[i]);
                }


            }


        };

        $scope.save = function(e,group,frm, index){


            var error = false;
            e.stopPropagation();
            if(!group.title || group.title.length === 0){
                frm['title' + index].$setValidity('required', false);
                error = true;
            }else{
                frm['title' + index].$setValidity('required', true);
            }


            group.options.forEach(function(option,i){
                var ii = 'option_select_' + index + i;

                if(option.priceOption === null){
                    console.log(ii);
                    frm[ii].$setValidity('required', false);
                    error = true;
                }else{
                    frm[ii].$setValidity('required', true);

                }
            });

            if(error){
                return;
            }
             if(group.id > 0){


                Network.put('priceoptiongroup', group)
                    .then(function(res){

                        group.options = res.options;
                    });

            }else{

                delete group.id;

                Network.post('priceoptiongroup', group)
                    .then(function(res){
                        console.dir(res);
                        group.id = res.id;
                        group.options = res.options;
                    });

            }


        };

        $scope.remove = function (option, group) {

            group.options.splice(_.findIndex(group.options, {id: option.id}), 1);

            if(group.id > 0){

                Network.remove('priceoptionitem', {id: option.id}, true);

                // Network.put('priceoptiongroup', group, true);

            }


         };

        $scope.cbClick = function(evt,o){
            evt.stopPropagation();
            if(!o.id){
                return evt.preventDefault();
            }

            Utils.handleDefaults(Option.groups, o, 'priceoptiongroup');

        };


        // Custom options
        $scope.addOption = function(frm){
            var has = $scope.customOptions.some(function(option){
                return option.id === -1;
            });
            if(!has){
                $scope.customOptions.unshift({id:-1, title:{ru:'', en:''}, description:{ru:'', en:''}});
            }

        };

        $scope.saveOption = function(evt, option, frm){
            evt.stopPropagation();
            var error = false,
                index = -1,
                isEn = /^[a-zA-Z0-9 ]*$/.test(option.title.en),
                i = _.findIndex($scope.customOptions, option);


            if(!option.title.ru || option.title.ru.length === 0){
                frm['title_ru' + i].$setValidity('required', false);
                error = true;

            }else{
                frm['title_ru' + i].$setValidity('required', true);
            }


            if(!option.title.en || !isEn){
                frm['title_en' + i].$setValidity('pattern', false);
                error = true;

            }else{
                frm['title_en' + i].$setValidity('pattern', true);
            }


            if(error){
                return;
            }



            if(option.id > 0){
                Network.put('priceoption', option)
                    .then(function(res){
                       index = _.findIndex(Option.options, {id:option.id});
                        Option.options[index] = res;
                    });

            }else{
                delete option.id;
                Network.post('priceoption', option)
                    .then(function(res){
                        Option.options.unshift(res.data);
                        $scope.customOptions[i] = res.data;


                    });
            }
        };

        $scope.destroyOption = function(evt, option, frm){
            evt.stopPropagation();
            var i = _.findIndex($scope.customOptions,{id:option.id});
            if(option.id > 0){
                var index = _.findIndex(Option.options, {id:option.id});
                Network.remove('priceoption', {id:option.id}, true)
                    .then(function(res){
                        Option.groups.forEach(function(group){
                            group.options.splice(_.findIndex(group.options, {priceOptionItem:option.id}),1);
                        });
                        Option.options.splice(index,1);

                    });

                $scope.customOptions.splice(i,1);
            }else{

               $scope.customOptions.splice(i,1);



            }






        };





    }])
    .service('Option', function($q,Network){
        return {
            groups:[],
            options:[],

            getOptions:function(){
                var that = this;

                return $q(function(resolve, reject){
                    Network.get('user')
                    .then(function(user){

                       //  Network.get('priceoption', {createdBy:user.id})
                       Network.get('priceoption/get')
                        .then(function(res){
                            that.options = res;
                            resolve(Option.options);
                        });


                    });
                   

                });

            },


            getGroups:function(){
                var that = this;
                return $q(function(resolve,reject){
                    Network.get('user')
                    .then(function(user){
                         Network.get('priceoptiongroup', {owner:user.id})
                        .then(function(res){
                            that.groups = res;
                            resolve(that.groups);
                        });


                    });
                   
                });
            }
        };
    });
