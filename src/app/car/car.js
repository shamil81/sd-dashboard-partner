/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 03/02/16
 */

angular.module('sd.car', ['ui.router', 'satellizer', 'constants', 'sdDashboard', 'ngMessages',
    'angularFileUpload', 'sdDashboard', 'sd.place', 'sd.requirement', 'sd.rentCondition', 'sd.option', 'utils', 'custom.ui'])
    .config(function ($stateProvider) {


        $stateProvider
            .state('car', {
                url: '/car',
                views: {
                    'main': {
                        templateUrl: 'car/car.tpl.html'
                    }
                },
                data: {
                    pageTitle: 'Автомобили'
                }

            })

            .state('car.profile', {
                url: '/profile/:id?clone',
                templateUrl: 'car/profile.tpl.html',
                params: {
                    car: null
                },
                data: {
                    pageTitle: 'Редактировать авто'
                }

            });


    })

    .controller('CarCtrl', function ($scope, $rootScope, $auth, $state, UserService, CarService, Helpers, Network) {


        $scope.state = $state;
        $scope.carsDisplay = [];
        $scope.common.showProgress = true;


        function chunk(arr, size) {
            var newArr = [],
                copy = angular.copy(arr);
            // Prepend an item for "Add button"
            copy.unshift({number: null});
            for (var i = 0; i < copy.length; i += size) {
                newArr.push(copy.slice(i, i + size));
            }
            return newArr;
        }


        CarService.getCars()
            .then(function () {

                $scope.collections = CarService.collections;


                updateDisplay();


            });


        function updateDisplay() {
            $scope.carsDisplay = chunk(CarService.cars, 4);
            console.dir($scope.carsDisplay);
            $scope.common.showProgress = false;
        }

        $rootScope.$on('updateDisplay', function () {
            updateDisplay();
        });


        $scope.onChange = function (car) {
            Network.put('car', CarService.normalize(car), true);
        };


        $scope.cloneCar = function (car) {


            $state.go('car.profile', {id: car.id, car:car, clone: true});


        };


    })
    .controller('CarProfileCtrl', function ($scope, $rootScope, $state, CarService, $sce, FileUploader, $auth, Place, Requirement, RentCondition, Price, $stateParams, Network) {


        $scope.imagesLeft = 5;

        $scope.car = {};
        $scope.selected = {
            years: []

        };


        $scope.next = function () {

            var idx = _.findIndex(CarService.cars, {id: $scope.car.id}) + 1,
                car = CarService.cars[idx];


            $state.go('car.profile', {id: car.id, car: car});


        };


        $scope.prev = function () {
            var idx = _.findIndex(CarService.cars, {id: $scope.car.id}) - 1,
                car = CarService.cars[idx];
            $state.go('car.profile', {id: car.id, car: car});


        };

        $scope.$watch('car', function (nV) {

            $scope.nav = CarService.handleNav(nV);

        });

        $scope.$watch('selected.model', function (nv, ov) {
            console.dir($scope.car);
            if (nv && nv.id != $scope.car.model.id) {
                $scope.car.model = CarService.collections['model'][nv.id];
            }
        }, true);

        if ($stateParams.car) {


            $scope.car = $stateParams.car;
            $scope.selected.maker = CarService.collections.maker[$scope.car.model.maker.id];
            setNumber($scope.car);
            setModel($scope.car);


        }

        if ($stateParams.clone && $stateParams.car) {
            $stateParams.car.images = [];
            setNumber();

        }

        if (!$stateParams.clone && $stateParams.id) {

            $scope.selected.isUpdate = true;


        }

        $scope.$watch('car.images', function (nV, oV) {
            if (nV) {
                $scope.imagesLeft = Math.abs($scope.car.images.length - 5);

            }
        }, true);


        if ($stateParams.id && !$scope.car.id) {

            var id = Number.parseInt($stateParams.id, 10);


            CarService.getCars()
                .then(function () {



                    $scope.car = _.find(CarService.cars, {id: id});



                    if ($stateParams.clone) {
                        $scope.car.images = [];
                    }


                    $scope.selected.maker = CarService.collections.maker[$scope.car.model.maker.id];


                    if ($stateParams.clone) {

                        $scope.car = _.omit($scope.car, ['number', 'createdAt', 'updatedAt', 'id']);

                    }


                    setModel($scope.car.model);

                    setNumber($scope.car);


                });

        }


        if (!$stateParams.car && !$stateParams.id) {


            // Set defaults

            CarService.getCars()
                .then(function () {
                    $scope.selected.maker = CarService.getDefault('maker');
                    $scope.car.model = CarService.collections['maker'][$scope.selected.maker.id]['carModel'][0];
                    $scope.car.location = CarService.getDefault('location');
                    $scope.car.pricePlan = CarService.getDefault('pricePlan');
                    $scope.car.customerRequirements = CarService.getDefault('customerRequirements');
                    $scope.car.rentConditions = CarService.getDefault('rentConditions');
                    $scope.car.priceOptionGroup = CarService.getDefault('priceOptionGroup');
                    $scope.car.color = CarService.getDefault('color');
                    $scope.car.fuel = CarService.getDefault('fuel');
                    $scope.car.wheelDrive = CarService.getDefault('wheelDrive');
                    $scope.car.isAutomatic = true;
                    $scope.car.status = CarService.getDefault('status');
                    $scope.car.conditioning = true;


                    setModel();

                    setNumber($scope.car);

                });

        }


        $scope.removeImage = function (car, image) {

            CarService.removeImage(car, image);


        };


        var uploader = $scope.uploader = new FileUploader({
            url: '/api/car/image',
            headers: {
                'Authorization': 'JWT ' + $auth.getToken()
            },
            alias: 'image',
            removeAfterUpload: true,
            autoUpload: false
        });

        uploader.onAfterAddingFile = function (item) {
            console.dir(item);
            if ($scope.car.id > 0) {
                uploader.uploadAll();
            }
        };

        /*      uploader.onAfterAddingAll = function (items) {
         console.log('all');
         };*/


        uploader.onBeforeUploadItem = function (item) {
            item.formData.push({car: $scope.car.id});

        };

        uploader.onSuccessItem = function (item, res) {

            if (!$scope.car.images) {
                $scope.car.images = [];
            }

            $scope.car.images.unshift(res);

        };

        function setNumber(car) {
            if (car && car.number) {
                var len = car.number.length,
                    // Either two or three digits eg. 78 or 178
                    region = (len === 9) ? -3 : -2;
                $scope.num = {
                    num1: car.number.substr(0, 1),
                    num2: car.number.substr(1, 3),
                    num3: car.number.substr(4, 2),
                    num4: car.number.substr(region)
                };

            } else {
                $scope.num = {
                    num1: 'A',
                    num2: '777',
                    num3: 'AA',
                    num4: '199'
                };
            }

        }

        function setModel() {
            //var model = _.find($scope.selected.maker.carModel, {id: id});

            $scope.selected.model = $scope.car.model;

            $scope.selected.years = CarService.getYears($scope.car.model);
        }

        $scope.submit = function (frm) {


            $scope.showInfo = false;
            $scope.err = false;
            $scope.errMsg = '';


            if ($scope.num.num1.toUpperCase() === 'A' && $scope.num.num2 === '777' && $scope.num.num3.toUpperCase() === 'AA' && $scope.num.num4 === '199') {


                frm.num.$setValidity('wrongNumber', false);

            } else if ($scope.num.num2.length !== 3) {
                frm.num.$setValidity('wrongNumber', false);

            } else if ($scope.num.num3.length !== 2) {
                frm.num.$setValidity('wrongNumber', false);

            } else {
                frm.num.$setValidity('wrongNumber', true);

            }

            if (!$scope.car.model) {
                frm.model.$setValidity('required', false);
            } else {
                frm.model.$setValidity('required', true);
            }

            if (!$scope.car.year) {
                frm.year.$setValidity('required', false);
            } else {
                frm.year.$setValidity('required', true);

            }

            if (frm.$invalid) {
                return;
            }
            var n = $scope.num;


            $scope.car.number = (n.num1 + n.num2 + n.num3 + n.num4).toUpperCase();


            var car = CarService.normalize($scope.car);


            if ($scope.selected.isUpdate) {

                var idx = _.findIndex(CarService.cars, {id: $scope.car.id});
                angular.merge(CarService.cars[idx], $scope.car);


                Network.put('car', car);
                $rootScope.$broadcast('updateDisplay');




            } else {
                if (car.id) {
                    delete car.id;

                }
                Network.post('car', car)
                    .then(function (res) {

                        $scope.car.id = res.data.id;

                        CarService.cars.unshift($scope.car);

                        $rootScope.$broadcast('updateDisplay');

                        $state.go('car.profile', {id: $scope.car.id, car: $scope.car}, {inherit: false});

                        if (!uploader.isUploading) {
                            uploader.uploadAll();
                        }


                    }, function (res) {
                        $scope.err = true;
                        if (res.data.invalidAttributes) {
                            var html = '';
                            for (var attr in res.data.invalidAttributes) {
                                html += res.data.invalidAttributes[attr][0]['message'] + '<br>';
                            }
                            $scope.errMsg = $sce.trustAsHtml(html);

                        }
                    });
            }


        };

    })
    .service('CarService', function (Network, $q, Place, RentCondition, Requirement, Price, Option, Helpers, $filter) {
        return {


            cars: [],


            // Stores dictionary data
            collections: {},

            getCollections: function () {
                var that = this;

                return $q(function (resolve, reject) {

                    if (Object.keys(that.collections).length > 0) {
                        return resolve(that.collections);
                    }


                    $q.all([
                        Network.get('carcolor'),
                        Network.get('fueltype'),
                        Network.get('wheeldrive'),
                        Network.get('carmaker'),
                        Requirement.getRequirements(),
                        RentCondition.getRentConditions(),
                        Price.getDailyPrice(),
                        Option.getGroups(),
                        Network.get('carstatus'),
                        Network.get('carmodel'),
                        Place.getLocations()


                    ])

                        .then(function (results) {

                            var doors = [
                                    {val: 2, word: 'двери'}, {val: 3, word: 'двери'},
                                    {val: 4, word: 'двери'},
                                    {val: 5, word: 'дверей'}],


                                o = {
                                    color: results[0],
                                    fuel: results[1],
                                    wheelDrive: results[2],
                                    maker: results[3],
                                    door: doors,
                                    //location: results[4],
                                    location: Place.locations,
                                    customerRequirements: results[4],
                                    rentConditions: results[5],
                                    pricePlan: results[6],
                                    priceOptionGroup: results[7],
                                    status: results[8],
                                    model: results[9]

                                };


                            that.collections = Helpers.sortCollection(o, ['door', 'location']);
                            that.collections.location = Place.locations;




                            resolve(that.collections);

                        }, function (err) {
                            reject(err);

                        });


                });

            },


            getCars: function () {


                var that = this;


                return $q(function (resolve) {

                    if (that.cars.length > 0) {
                        return resolve(that.cars);
                    }


                    that.getCollections()
                        .then(function () {
                            Network.get('car/my')
                                .then(function (data) {


                                    that.cars = (function (cars) {

                                        var arr = [];

                                        cars.forEach(function (car, i) {

                                            var o = angular.copy(car);

                                            _.forOwn(o, function (val, key) {


                                                if (that.collections[key]) {

                                                    o[key] = that.collections[key][val];


                                                }

                                            });


                                            arr.splice(i, 0, o);

                                        });

                                        return $filter('orderBy')(arr, 'id', true);

                                    })(data);

                                    resolve(that.cars);
                                });

                        });

                });
            },

            getYears: function (o) {
                var model = {};
                angular.copy(o, model);
                var year = Number.parseInt(model.produceStart),
                    end = Number.parseInt(model.produceEnd),
                    years = [year];

                while (year < end) {
                    year++;
                    years.push(year.toString());
                }

                return years;

            },

            getDefault: function (key) {

                var item = _.find(this.collections[key], {isDefault: true});
                if (item) {
                    return item;
                }
                if (this.collections[key]) {
                    return this.collections[key][Object.keys(this.collections[key])[0]];
                }

                return null;
            },

            removeImage: function (car, image) {

                var i = _.findIndex(car.images, image);
                if (i >= 0) {
                    Network.remove('car/image', {id: image.id})
                        .then(function () {
                            car.images.splice(i, 1);
                        });
                }
            },

            normalize: function (car) {

                var o = {};

                _.forOwn(car, function (val, key) {

                    if (val && typeof val === 'object') {

                        o[key] = Number.parseInt(val.id);

                    } else {

                        o[key] = val;
                    }


                });

                return o;


            },

            isFirst: function (car) {
                return _.findIndex(this.cars, {id: car.id}) === 0;
            },

            isLast: function (car) {
                return !(this.cars[_.findIndex(this.cars, {id: car.id}) + 1]);
            },

            handleNav: function (car) {


                var o = {
                    next: false,
                    prev: false
                };

                if (car.id) {
                    o.next = !(this.isLast(car));
                    o.prev = !(this.isFirst(car));
                }

                return o;

            }


        };
    })
    .directive('frm', function () {

        return {
            restrict: 'E',
            link: function (scope, elem) {
                elem.on('submit', function () {
                    scope.$broadcast('form:submit');
                });
            }
        };

    })
    .directive('carNumber', function () {
        return {
            require: '?ngModel',

            templateUrl: 'car/number.tpl.html',
            link: function (scope, el, attrs, frm) {

                var number, re = new RegExp('[^УКЕНХВАРОСМТETYOPAHKXCBM]', 'g'), ip1keypress = false, ip2keypress = false, ip4keypress = false, ip3keypress = false, fines = 0, summ = 0, cnt = 0;
                $(function () {
                    var data = [[[1, 100, 1000], [4, 110, 500], [3, 148, 300], [2, 30, 0]],  // 3 = 1800
                        [[3, 65, 100], [2, 98, 1000], [2, 120, 300], [4, 105, 100]],  // 4 = 1500
                        [[2, 200, 1500], [5, 168, 500], [3, 20, 0]],  // 2 = 2000
                        [[5, 288, 500], [5, 100, 0]],  // 1 = 500
                        [[3, 120, 100], [3, 140, 1000], [3, 118, 800], [1, 10, 0]],  // 3 = 1900
                        [[3, 110, 100], [4, 248, 1500], [3, 30, 0]],  // 2 = 1600
                        [[5, 148, 100], [2, 140, 100], [3, 100, 0]],  // 2 = 200
                        [[3, 120, 300], [2, 88, 1000], [3, 80, 500], [2, 100, 0]],  // 3 = 1600
                        [[4, 150, 500], [2, 168, 100], [4, 70, 0]],  // 2 = 600
                        [[1, 140, 100], [5, 200, 1000], [4, 48, 0]],  // 2 = 1100
                        [[2, 150, 100], [4, 180, 100], [4, 58, 0]]];  // 2 = 200

                    $('.ip1').bind({
                        focus: function () {
                            if (!ip1keypress && $(this).val() == 'A') {
                                $(this).val('');
                            }
                            $(this).css({'color': '#231f20', 'border': '#0054c6 solid 1px'});
                        },
                        blur: function () {
                            if (!$(this).val().length) {
                                ip1keypress = false;
                                $(this).val('A');
                            }
                            $(this).css({'color': '#959595', 'border': '#b5b4b4 solid 1px'});
                            if (ip1keypress) {
                                $(this).css({'color': '#231f20'});

                            }
                        },
                        keyup: function () {
                            ip1keypress = true;
                            $(this).val($(this).val().toUpperCase().replace(re, ''));
                        }
                    });

                    $('.ip2').bind({
                        focus: function () {
                            if (!ip2keypress && $(this).val() == '777') {
                                $(this).val('');
                            }
                            $(this).css({'color': '#231f20', 'border': '#0054c6 solid 1px'});
                        },
                        blur: function () {
                            if (!$(this).val().length) {
                                ip2keypress = false;
                                $(this).val('777');
                            }
                            $(this).css({'color': '#959595', 'border': '#b5b4b4 solid 1px'});
                            if (ip2keypress) {
                                $(this).css({'color': '#231f20'});

                            }
                        },
                        keyup: function () {
                            ip2keypress = true;
                            $(this).val($(this).val().replace(/\D/g, ''));
                        }
                    });

                    $('.ip3').bind({
                        focus: function () {
                            if (!ip3keypress && $(this).val() == 'AA') {
                                $(this).val('');
                            }
                            $(this).css({'color': '#231f20', 'border': '#0054c6 solid 1px'});
                        },
                        blur: function () {
                            if (!$(this).val().length) {
                                ip3keypress = false;
                                $(this).val('AA');
                            }
                            $(this).css({'color': '#959595', 'border': '#b5b4b4 solid 1px'});
                            if (ip3keypress) {
                                $(this).css({'color': '#231f20'});
                            }
                        },
                        keyup: function () {
                            ip3keypress = true;
                            $(this).val($(this).val().toUpperCase().replace(re, ''));
                        }
                    });

                    $('.ip4').bind({
                        focus: function () {
                            if (!ip4keypress && $(this).val() == '199') {
                                $(this).val('');
                            }
                            $(this).css({'color': '#231f20', 'border': '#0054c6 solid 1px'});
                        },
                        blur: function () {
                            if (!$(this).val().length) {
                                ip4keypress = false;
                                $(this).val('199');
                            }
                            $(this).css({'color': '#959595', 'border': '#b5b4b4 solid 1px'});
                            if (ip4keypress) {
                                $(this).css({'color': '#231f20'});

                            }
                        },
                        keyup: function () {
                            ip4keypress = true;
                            $(this).val($(this).val().toUpperCase().replace(/\D/g, ''));
                        }
                    });

                    scope.$on('form:submit', function () {
                        var ip1 = $(".ip1").val().replace(re, '');
                        var ip2 = $(".ip2").val().replace(/\D/g, '');
                        var ip3 = $(".ip3").val().replace(re, '');
                        var ip4 = $(".ip4").val().replace(/\D/g, '');

                        if (ip1 == 'A' && ip2 == '777' && ip3 == 'AA' && ip4 == '199') {
                            frm.num.$setValidity('wrongNumber', false);
                            ip1.css('background-color', 'red');
                            ip2.css('background-color', 'red');
                            ip3.css('background-color', 'red');
                            ip4.css('background-color', 'red');


                            return false;
                        }
                        if (!ip1) {
                            alert('Номер автомобиля указан неверно');
                            $(".ip1").effect('highlight', {'color': '#A00'});
                            return false;
                        }
                        if (ip2.length != 3) {
                            alert('Номер автомобиля указан неверно');
                            $(".ip2").effect('highlight', {'color': '#A00'});
                            return false;
                        }
                        if (ip3.length != 2) {
                            alert('Номер автомобиля указан неверно');
                            $(".ip3").effect('highlight', {'color': '#A00'});
                            return false;
                        }

                        if (ip4.length < 2 || parseInt(ip4, 10) === 0 || parseInt(ip4, 10) > 199 || parseInt(ip4, 10) === 100) {
                            alert('Номер автомобиля указан неверно');
                            $(".ip4").effect('highlight', {'color': '#A00'});
                            return false;
                        }
                        $('.number-container input').attr('disabled', 'disabled');
                        number = ip1 + ip2 + ip3 + ip4;
                        //$('.datanumber').html(number);

                        var KEY = (ip2 * 1 + ip4 * 1 + ip1.charCodeAt(0) * 1 + ip3.charCodeAt(0) * 1 + ip3.charCodeAt(1) * 1);
                        data = data[KEY % data.length];
                        //$('#tab31 .d3 .anim').animate({"width": "55px"}, 1500, anim);
                        return false;
                    });

                });


            }
        };
    })
    .filter('carnumber', function () {
        return function (num) {

            var strAr = num.split('');

            strAr.splice(1, 0, ' ');
            strAr.splice(5, 0, ' ');
            strAr.splice(8, 0, ' ');
            return strAr.join('');


        };
    });


