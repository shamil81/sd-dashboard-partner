angular.module('sd.rentCondition',['ui.router', 'satellizer', 'sdDashboard', 'ngMessages', 'angularFileUpload', 'sdDashboard', 'ngMaterial'])
    .config(function ($stateProvider) {
        $stateProvider
            .state('rentcondition', {
                url: '/rentcondition',
                views: {
                    'main': {
                        templateUrl: 'rentCondition/rentCondition.tpl.html'

                    }
                },
                controller: 'RentConditionCtrl',
                data: {
                    pageTitle: 'Условия аренды'
                }
            });


    })
.controller('RentConditionCtrl', function($scope, Network, Utils, RentCondition){

    var condition = {title:{ru:'',en:''}};

    RentCondition.getRentConditions()
        .then(function(){
            $scope.conditionGroups = RentCondition.rentConditions;
        });




    $scope.addGroup = function(){
        var group = {
            title:'',
            conditions:[{title:{'ru':'', en:''}}]

        };

        $scope.conditionGroups.unshift(group);

    };

    $scope.add = function(e,index){
        e.stopPropagation();
        var c = RentCondition.rentConditions[index]['conditions'];
        console.dir(c);
        if(c.length === 0 || (c[0]['title']['ru'].length !== 0 && c[0]['title']['en'].length !== 0)){
            c.unshift({title:{ru:'',en:''}});
         }
    };

    $scope.destroyGroup = function(e,group){
        e.stopPropagation();


        if(group.hasOwnProperty('id')){
            Network.remove('RentConditionGroup', {id:group.id});

        }
        var index = _.findIndex(Requirement.requirements, group);

        RentCondition.rentConditions.splice(index,1);
        if(group.isDefault && RentCondition.rentConditions.length > 0){

            var i = _.findIndex(RentCondition.rentConditions, function(item){
               return item.id > 0;
            });

            if(i > 0){
                RentCondition.rentConditions[i].isDefault = true;
                Netowrk.put('RentConditionGroup', RentCondition.rentConditions[i]);
            }


        }




    };

    $scope.onKeyDown = function(evt){
        evt.stopPropagation();

    };


    $scope.save = function(e,group, frm, index){
        e.stopPropagation();
        var error = false;



        if(group.title.length === 0){
            frm['title' + index].$setValidity('required', false);
            error = true;
        }else{
            frm['title' + index].$setValidity('required', true);
        }

        group.conditions.forEach(function(condition, i){
            console.dir(condition);
            if(!condition['title']['ru'] || condition['title']['ru'].length === 0){
                frm['title_ru' + index + i].$setValidity('required', false);
                error = true;


            }else {
                frm['title_ru' + index + i].$setValidity('required', true);
            }

            if(!condition['title']['en'] || condition['title']['en'].length === 0 || /^[a-zA-Z0-9 ]*$/.test(condition['title']['en']) === false){
                frm['title_en' + index + i].$setValidity('pattern', false);
                error = true;


            }else{
                frm['title_en' + index + i].$setValidity('pattern', true);

            }


        });

        if(error){
            return;
        }


        if(group.hasOwnProperty('id')){
            var o = {};
            angular.copy(group,o);
            o.owner = o.owner.id;
            o = _.pick(o, ['title','conditions','owner', 'id']);

            Network.put('rentconditiongroup'+'/' + o.id, group);

        }else{
            Network.post('rentconditiongroup', group)
                .then(function(res){
                    group.id = res.data.id;
                });
        }



    };

    $scope.cbClick = function(evt,o){
        evt.stopPropagation();
        if(!o.id){
            return evt.preventDefault();
        }

        Utils.handleDefaults(RentCondition.rentConditions, o, 'rentconditiongroup');

    };

    $scope.remove = function(group, index){



        var removed = group.conditions.splice(index,1);



        if(removed[0].title.ru.length === 0 && removed[0].title.en.length === 0){
            return;
        }




        if(group.hasOwnProperty('id')){
            Network.put('rentconditiongroup', group, true);
        }
    };







})
.service('RentCondition', function($q,Network, Utils){
    return {
        rentConditions:[],
        getRentConditions:function(){
            var that = this;
            return $q(function(resolve){
                Network.get('user')
                    .then(function(user){
                        Network.get('rentconditiongroup')
                            .then(function(res){
                                res.forEach(function(group){
                                    group.conditions.forEach(function(c,i){

                                        if(Utils.isJSON(c)){
                                            group.conditions[i] = JSON.parse(c);
                                        }

                                    });

                                });

                                console.dir(res);

                                that.rentConditions = res;



                                return resolve(res);
                            });
                    });
            });

        }

    };
});