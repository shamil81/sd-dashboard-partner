angular.module('sd.price', ['ui.router', 'satellizer', 'sdDashboard', 'ngMessages', 'angularFileUpload', 'sdDashboard', 'vAccordion', 'ngMaterial'])
    .config(function ($stateProvider) {
        $stateProvider
            .state('price', {
                url: '/price',
                views: {
                    'main': {
                        templateUrl: 'price/price.tpl.html'

                    }
                },
                controller: 'PriceCtrl',
                data: {
                    pageTitle: 'Тарифные планы'
                }
            });


    })

    .controller('PriceCtrl', function ($scope, Network, Price, $timeout) {


        $scope.common.showProgress = true;


        Price.getPricePlan()
                    .then(function(){
                        $scope.priceTypes = Price.planTypes;
                        $scope.plans = Price.plans;
                        $scope.common.showProgress = false;
                    });



        $scope.onPlanSelect = function (plan) {


            Price.preparePlan(plan, function (res) {

                //angular.copy(res['price'],plan['price']);
                plan.ready = true;

                console.dir($scope.plans);


            });


        };

        $scope.onPriceSelect = function (plan, key) {


            var prices = plan.price,
                selected = prices[key].selected;


            if (key !== selected) {


                Object.defineProperty(prices, selected, Object.getOwnPropertyDescriptor(prices, key));

                delete prices[key];

                Price.preparePlan(plan, function (res) {
                    plan['price'] = res['price'];
                }, true);

            }


        };


        $scope.savePlan = function (evt, plan, frm, index) {

            evt.stopPropagation();
            var title = 'title' + index;
            if (plan.title.length === 0) {
                frm[title].$setValidity('required', false);
                return;
            } else {
                frm[title].$setValidity('required', true);

            }


            for (var key in plan.price) {

                var unlimited = 'unlimited' + key + index,
                    limited = 'limited' + key + index,
                    discount = 'discount' + key + index;


                if (Number.parseInt(key) === 0) {
                    frm['select' + key + index].$setValidity('required', false);
                    return;

                } else {
                    frm['select' + key + index].$setValidity('required', true);

                }

                if (Price.invalid(plan.price[key]['unlimited'])) {
                    frm[unlimited].$setValidity('required', false);
                    return;
                } else {
                    frm[unlimited].$setValidity('required', true);

                }

                if (Price.invalid(plan.price[key]['limited'])) {
                    console.log(limited);
                    frm[limited].$setValidity('required', false);
                    return;

                } else {
                    frm[limited].$setValidity('required', true);

                }

                var d = Number.parseInt(plan.price[key]['discount']);


                if (Number.isNaN(d) || d < 0 || d >= 100) {

                    frm[discount].$setValidity('required', false);

                } else {
                    frm[discount].$setValidity('required', true);
                }


            }


            Network.post('priceplan', Price.sanitizePlan(plan))
                .then(function (data) {
                    plan.id = data.id;
                });


        };

        $scope.destroyPlan = function (e, plan, index) {
            e.stopPropagation();
            if (plan.hasOwnProperty('id')) {
                Network.remove('priceplan', {id: plan.id});
            }
            $scope.plans.splice(index, 1);
        };

        $scope.addPlan = function (priceType) {

            $scope.plans.unshift({
                title: '',
                discount: 0,
                runLimit: 200,
                overrun: 0,
                price: {},
                pricePlanType: priceType,
                ready: true
            });

        };

        $scope.add2Plan = function (e, plan) {

            e.stopPropagation();


            var index = 0;


            if (plan.price.hasOwnProperty(index.toString())) {
                 return;
            }



            plan.price[index] = {

                discount: 0,
                limited: 0,
                unlimited: 0,
                selected: index

            };

            $timeout(function () {
                Price.preparePlan(plan, function (res) {
                    plan['price'] = res['price'];
                        plan.ready = true;
                }, true);


            }, 300);

        };


        $scope.removePrice = function (plan, key) {

            delete plan.price[key];

            if (plan.hasOwnProperty('id') && Number.parseInt(key) !== 0) {
                Network.post('priceplan', Price.sanitizePlan(plan));

            }

            Price.preparePlan(plan, function (res) {
                plan['price'] = res['price'];
            }, true);

        };





    })
    .service('Price', function ($q, Network) {
        return {

            plans: [],
            planTypes:[],

            getPricePlan:function(){
                var that = this;
                return $q(function(resolve, reject){
                    Network.get('user')
                        .then(function(user){
                            Network.get('priceplan', {owner:user.id})
                                .then(function (res) {
                                    console.dir(res);
                                    Network.get('pricePlanType')
                                        .then(function(planTypes){
                                            that.planTypes = planTypes;
                                            res.forEach(function(plan){
                                                planTypes.forEach(function(planType){
                                                    if(plan.pricePlanType === planType.id){
                                                        plan.pricePlanType = planType;
                                                    }
                                                });
                                            });
                                            that.planTypes = planTypes;
                                            that.plans = res;
                                            resolve(that.plans);
                                        });



                                });

                        });

                });
            },

            preparePlan: function (plan, callback) {

                var possibleValues = [],
                    existingValues = Object.keys(plan.price),
                    min = plan.pricePlanType.min,
                    max = plan.pricePlanType.max;


                async.whilst(
                    function () {
                        return max >= min;
                    }, function (cb) {
                        possibleValues.push(min);
                        min++;
                        cb();

                    }, function () {
                        async.forEachOf(plan.price, function (price, i, cb1) {
                            var values = possibleValues.slice();


                            async.forEachOf(existingValues, function (value, ii, cb2) {
                                var existing = Number.parseInt(existingValues[ii]);

                                if (existing !== Number.parseInt(i)) {
                                    values.splice(values.indexOf(existing), 1);
                                }
                                cb2();
                            }, function (err) {
                                plan.price[i]['selected'] = Number.parseInt(i);
                                console.log(plan.price[i]['selected']);
                                plan.price[i]['possibleValues'] = values;
                                console.dir( plan.price[i]['possibleValues']);
                                console.log('---------------------------');


                                cb1();
                            });


                        }, function (err) {
                            async.setImmediate(function () {
                                callback(plan);
                            });
                        });
                    });


            },

            // Use only daily price plan for now

            getDailyPrice:function(){
                return $q(function(resolve,reject){
                    Network.get('user')
                    .then(function(user){


                         Network.get('priceplan', {pricePlanType:1, owner:user.id})
                        .then(function(res){
                            resolve(res);
                        });


                    });
                   
                });
            },


            invalid: function (price) {
                return isNaN(price) || price <= 0;
            },


            sanitizePlan: function (plan) {
               var o = angular.copy(plan);
               delete o.cars;
               delete o.owner;
                o.pricePlanType = plan.pricePlanType.id;
                return o;
            }
        };
    });
