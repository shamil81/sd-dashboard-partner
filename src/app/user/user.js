angular.module('sd.user', ['ui.router', 'sdDashboard'])
    .config(function ($stateProvider){
        $stateProvider
            .state('user',{
                url: '/me',
                views: {
                    'main': {
                        templateUrl: 'user/user.tpl.html'

                    }
                },
                controller: 'UserCtrl',
                data: {
                    pageTitle: 'Профиль партнера'
                }
            });

    })
    .controller('UserCtrl', function($scope, UserService, $filter){

        UserService.get(function(user){
            if(user.birthDate){
                user.birthDate = $filter('date')(user.birthDate, "dd'.'MM'.'yyyy");

            }
            if(user.passportDate){
                user.passportDate =  $filter('date')(user.passportDate, "dd'.'MM'.'yyyy");
            }
           $scope.partner = user;
        });



    });
