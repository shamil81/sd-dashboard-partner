
/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 16/01/16
 */
angular.module('sd.confirm',['ui.router', 'satellizer', 'sdDashboard'])
    .config(function ($stateProvider) {



        $stateProvider
            .state('confirm', {
                url:'/confirm/:hash',
                views:{
                    'main':{
                        templateUrl:'confirm/confirm.tpl.html'
                    }
                },
                data:{
                    pageTitle: 'Подтверждение'
                }

            });

    })
    .controller('ConfirmCtrl', function ($scope, $state, $stateParams, $rootScope, $http, $auth, Network) {
        $scope.frm = false;
        var user = {};
        Network.get('confirm', {hash: $stateParams.hash}, true)
            .then(function(res){
                console.dir(res);


                if (res.user && res.user.confirmed === false) {
                    angular.copy(res.user, $scope.user);
                    $scope.user.hash = $stateParams.hash;
                    $scope.frm = true;

                }else if($auth.isAuthenticated()){
                    $state.go('main');
                }else{
                    $state.go('login');
                }

            }, function(err){
                //alert(err);
            });
        $scope.submit = function(frm){
            if(frm.$invalid){
                return;
            }

            $http.post('/api/confirm', $scope.user)
                .then(function(res){
                    $scope.user.userType = 1;

                    $scope.user.email = res.data.email;
                    $auth.login($scope.user)
                        .then(function(res){
                            console.dir(res);
                            $auth.setToken(res.data.token);
                            console.dir(res.data);
                           $state.go('main', res.data);
                        }, function(err){
                            console.error(err);
                        });
                });

        };
    });
