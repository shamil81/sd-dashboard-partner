angular.module('constants', [])
    .constant('baseUrl', '/')
    .constant('uploadsUrl', '/assets/uploads')

    .constant('config', {

        'businessHours':{
            // Minimum hours (in milliseconds) between start and end of Location's business days (eg. 3600000 - for an hour)
            'hourDelta':7200000,

            'step':60
        }


    });
