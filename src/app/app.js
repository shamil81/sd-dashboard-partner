angular.module('sdDashboard', [
        'templates-app',
        'templates-common',
        'ui.router',
        'ngAnimate',
        'ngAria',
        'ngMessages',
        'ngMaterial',
        'satellizer',
        'formly',
        'sd.auth',
        'sd.confirm',
        'sd.car',
        'sd.rentCondition',
         'sd.price',
         'sd.place',
         'sd.requirement',
         'sd.option',
         'sd.user',
    'utils'
    ])

    .config(function myAppConfig($stateProvider, $urlRouterProvider, $mdThemingProvider, $locationProvider, $sceProvider) {
        $urlRouterProvider.otherwise('/');
        $mdThemingProvider.theme('default')
            .primaryPalette('red')
            .accentPalette('yellow');

        $locationProvider.html5Mode(true);

        $sceProvider.enabled(false);


        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('main', {
                url:'/',
                data:{
                    pageTitle:'Главная'
                }


               /* controller:function($scope, $auth, $state){
                    if(!$auth.isAuthenticated()){
                        $state.go('login');;
                    }
                }
*/
            });




    })




    .run(function ($rootScope, $location) {

        $(document).ready(function (config) {



        });

    })



    .controller('AppCtrl', function AppCtrl($scope, $location, $auth, $state, $http, AppService, $mdDialog, UserService) {


        $scope.common = {
            showProgress: false
        };

        $scope.openMenu = function ($mdOpenMenu, evt) {
            $mdOpenMenu(evt);
        };

        $scope.logout = function(){
            $auth.logout().then(function(){
                UserService.user = null;
                $state.go('login');
            });
        };

        if($location.path().substr(0,8) !== '/confirm' && !$auth.isAuthenticated()){
            console.log($location.path());
            $state.go('login');
        }

        $scope.user = {};

        $scope.error = {
            show:false
        };


        if($auth.isAuthenticated() /*&& !$scope.user.email*/){

            UserService.get(function(user){
                $scope.user = UserService.user;
                console.dir($scope.user);

            });

        }



        // App wide notifiction (Moved to Network service)

     /*   $scope.$on('showToast', function (e, param) {

            AppService.showToast(param);


        });*/


        $scope.checkIfOwnPage = function () {

            if($state.current.name === 'confirm'){
                return true;
            }

            return _.contains(["/404", "/pages/500", "/login", "/pages/signin", "/pages/signin2", "/pages/signup", "/pages/signup1", "/pages/signup2", "/pages/forgot", "/pages/lock-screen"], $location.path());

        };

        $scope.userDisplay = {
            theme_name: "Kimono",
            user_name: "John Doe"
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | Selectdrive';
            }

        });


    })
    .controller("NavCtrl", function ($scope) {

            $scope.navInfo = {
                tasks_number: 5,
                widgets_number: 13
            };

            $scope.toggleAlternativeMenu = function () {
                $('body .page-wrapper').toggleClass('nav-style--alternative');
            };

        })
    .controller('RightNavbarCtrl', function ($scope, $timeout, $mdSidenav, $log) {
        $scope.close = function () {
            $mdSidenav('rightmessages').close()
                .then(function () {
                    $log.debug("close RIGHT is done");
                });
        };

        $scope.toggleRightNavbar = function() {
            $mdSidenav('rightmessages').toggle()
                .then(function(){
                    $log.debug("toggle RIGHT is done");
                });
        };
    })
    .directive("toggleMinNav",
        function($rootScope) {
            return {
                link: function(scope, ele) {
                    var $content, $nav, $window, Timer, app, updateClass;

                    return app = $("#app"), $window = $(window), $nav = $("#nav-container"), $content = $("#content"), ele.on("click", function(e) {

                        if(app.hasClass("nav-min")){
                            app.removeClass("nav-min");
                        }
                        else{
                            app.addClass("nav-min");
                            $rootScope.$broadcast("minNav:enabled");
                            e.preventDefault();
                        }

                    }), Timer = void 0, updateClass = function() {
                        var width = $window.width();
                        return (980 > width && !app.hasClass("on-canvas")) ? app.addClass("nav-min") : void 0;
                    },initResize = function() {
                        var width = $window.width();
                        return 980 > width ? (app.hasClass("nav-min") ? "" : app.addClass("nav-min")) : (app.hasClass("nav-min")? "" : app.removeClass("nav-min"));
                    }, $window.resize(function() {
                        var t;
                        return clearTimeout(t), t = setTimeout(updateClass, 300);
                    }),initResize();

                }
            };
        })
    .directive("toggleOffCanvas", [
        function() {
            return {
                link: function(scope, ele) {
                    return ele.on("click", function() {
                        return $("#app").toggleClass("on-canvas").toggleClass("nav-min");
                    });
                }
            };
        }
    ]).directive("slimScroll", [
    function() {
        return {
            link: function(scope, ele, attrs) {
                return ele.slimScroll({
                    height: attrs.scrollHeight || "100%"
                });
            }
        };
    }
]).directive("goBack", [
    function() {
        return {
            restrict: "A",
            controller: ["$scope", "$element", "$window",
                function($scope, $element, $window) {
                    return $element.on("click", function() {
                        return $window.history.back();
                    });
                }
            ]
        };
    }
])
    .directive('passwordCheck', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.passwordCheck;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('passwordmatch', v);
                    });
                });
            }
        };
    })
    .directive('integer', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var INTEGER_REGEXP = /^\-?\d+$/;

                ctrl.$validators.integer = function(modelValue, viewValue) {
                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty models to be invalid
                        return false;
                    }

                    if (INTEGER_REGEXP.test(viewValue)) {
                        // it is valid
                        return true;
                    }

                    // it is invalid
                    return false;
                };
            }
        };
    })


    .directive('chooseFileButton', function() {
        return {
            restrict: 'E',
            link: function (scope, elem) {
                var button = elem.find('button');
                var input = elem.find('input');
                input.css({ display:'none' });
                button.bind('click', function() {
                    input[0].click();
                });
            }
        };
    })
    .service('UserService', function($http){


        return {
            user:null,
            get:function(cb){
              var that = this;
                if(!that.user){
                    $http.get('/api/user')
                        .then(function(res){
                            //angular.extend($scope.user, res.data);
                            that.user = res.data;
                            cb(res.data);
                        }, function(res){
                            console.error(res);
                        });
                }else{
                    cb(that.user);
                }

            }
        };

    })

    .service('Network', function($http, AppService, $q){
        return {
            cache:{},

            // Defaults to serve cached object
            get:function(resource, options, invalidateCache){

                var key = resource + JSON.stringify(options);
                options = options || {};

                if(this.cache[key] && !invalidateCache){
                    return this.cache[key];
                }

                return this.cache[key] = $http({
                    url:'/api/' + resource,
                    method:'GET',
                    params:options
                })
                    .then(function(response){
                        return response.data;
                    });


            },

            post:function(resource, data, noToast){
                return $http.post('/api/' + resource, data)
                    .then(function(response){

                        if (response.status !== 200 && response.status !== 201) {
                            return response;
                        }

                        if(noToast){
                            return response.data;
                        }
                        AppService.showToast({action:'save'});
                        return response.data;

                    });
            },

            put:function(resource, data, noToast){
                return $http.put('/api/' + resource, data)
                    .then(function(response){
                        if(noToast){
                            return response.data;
                        }
                        console.dir(response);
                        AppService.showToast({action:'save'});

                        return response.data;
                    }, function(err){
                        return $q.reject(err);
                    });

            },

            remove:function(resource,options){
               return $http({
                   url:'/api/' + resource,
                   method:'DELETE',
                   params:options

               })
                   .then(function(response){
                       return response.data;
                   });
            }


        };
    })
    .controller('ToastCtrl', function ($scope, $mdToast, $mdDialog, text) {
        var isDlgOpen;

        $scope.text = text;

        $scope.closeToast = function () {
            if (isDlgOpen) {
                return;
            }
            $mdToast
                .hide()
                .then(function () {
                    isDlgOpen = false;
                });
        };
        $scope.openMoreInfo = function (e) {
            if (isDlgOpen) {
                return;
            }
            isDlgOpen = true;
            $mdDialog
                .show($mdDialog
                    .alert()
                    .title('More info goes here.')
                    .textContent('Something witty.')
                    .ariaLabel('More info')
                    .ok('Got it')
                    .targetEvent(e)
                )
                .then(function () {
                    isDlgOpen = false;
                });
        };
    })
    // Formats user's display name based on isLegalEntity property value
    .filter('display', function(){
        return function (user){
            if(user.isLegalEntity){
                return user.legalEntityName;
            }
            var middleName = ' ';
            if(user.middleName && user.middleName.length > 0){
                middleName = ' ' + user.middleName + ' ';
            }
            return user.firstName + middleName + user.lastName;

        };


    })
    .filter('to', function(){
        return function (user){
            if(user.isLegalEntity){
                return user.legalEntityName;
            }
            var middleName = ' ';
            if(user.middleName && user.middleName.length > 0){
                middleName = ' ' + user.middleName + ' ';
            }
            return user.firstName + middleName + user.lastName;

        };


    })
    .filter('isEmpty', function(){
        return function (o){
            if(!o){
                return true;
            }

            if(Array.isArray(o)){
                return o.length === 0;
            }
            return Object.keys(o).length === 0;
        };
    })
    .service('AppService', function($mdToast){
        return {
            showToast:function(param){
                var locals = {text: 'Сохранено'};

                switch (param.action) {
                    case 'destroy':
                        locals.text = 'Удалено';
                        break;


                }

                $mdToast.show({
                    hideDelay:3000,
                    position: 'top right',
                    controller: 'ToastCtrl',
                    theme: 'default',
                    locals: locals,
                    templateUrl: 'ui-components/toast.tpl.html',
                    bindToController: true

                });

            }
        };
    })
    .service('Utils', function(Network){
        return {
            isJSON:function(str){
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            },

            handleDefaults:function(collection, item, apiUrl){
                if(item.isDefault){
                    return;
                }

                _.forEach(collection, function (stuff) {

                    if (item.id === stuff.id) {
                        return stuff.isDefault = true;
                    }

                    stuff.isDefault = false;
                });


                Network.put(apiUrl, item, true);
            }
        };
    })
    .service('Notify', function($rootScope) {
        return {
            subscribe: function(name, scope, callback) {
                var handler = $rootScope.$on(name, callback);
                scope.$on('$destroy', handler);
            },

            emit: function(evtName,data) {
                $rootScope.$emit(evtName, data);
            }
        };
    });





