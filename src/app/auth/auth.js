angular.module('sd.auth', ['constants', 'satellizer', 'ui.router'])

    .config(function($stateProvider,$authProvider, baseUrl){

        $stateProvider
            .state('login',{
                url:'/login',
                views: {

                    'main':{
                        templateUrl:'auth/login.tpl.html'

                    }

                },
                data:{pageTitle:'Вход'}

            });
        $authProvider.baseUrl = baseUrl;
        $authProvider.loginUrl = '/api/auth';
        $authProvider.signupUrl = '/api/user';
        $authProvider.tokenPrefix = 'selectdrive_pa';
        $authProvider.authToken = 'JWT';
        $authProvider.tokenName = '';


    })
    .filter('btnTitle', function () {
        return function (input) {
            if (input) {
                return 'Отправить';
            }
            return 'Вход';
        };
    })
    .controller('AuthCtrl', function ($scope, $state, $auth, Network) {

        $scope.submit = function(frm){
            $scope.wrongCredentials = false;
            $scope.showResetInfo = false;
            $scope.err = false;

            if(frm.$invalid){
                return;
            }


            if ($scope.isForgot) {
                Network.post('auth/reset', {email: $scope.user.email}, true)
                    .then(function (res) {
                        $scope.showResetInfo = true;
                    }, function (res) {
                        if (res.status === 401 || res.status === 404) {

                            $scope.wrongCredentials = true;

                        } else {
                            $scope.err = true;
                        }
                    });
            } else {
                $auth.login({email: $scope.user.email, password: $scope.user.password, userType: 1})

                    .then(function (res) {
                            console.log(res.data);
                            angular.extend($scope.user, res.data.user);
                            $auth.setToken(res.data.token);

                            $state.go('main');


                        },
                        function (res) {

                            if (res.status === 401 || res.status === 404) {

                                $scope.wrongCredentials = true;

                            } else {
                                $scope.err = true;
                            }

                        });

            }


        };


        $scope.forgot = function () {


            $scope.isForgot = true;

        };

    });

