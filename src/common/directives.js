angular.module('custom.ui', [])
    .directive('businessHours', function ($compile) {
        return {

            restrict: 'AE',


            scope: {
                model: '=ngModel',

                onChange: '&',

                // Minimum hours (in milliseconds) between start and end
                hourDelta: '@',

                step: '@',

            },


            link: function (scope, $el) {


                var theScope,
                    step = Number.parseInt(scope.step),
                    delta = Number.parseInt(scope.hourDelta),
                    toLocDate = function (date) {
                        return new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
                    },
                    maxStart = toLocDate(new Date(new Date(86400000).getTime() - delta)),
                    maxStartHours = maxStart.getHours(),
                    maxStartMinutes = maxStart.getMinutes(),
                    minEnd = toLocDate(new Date(delta)),

                    startConfig = {
                        'timeFormat': 'H:i',
                        'step': step,
                        'disableTextInput': true,
                        'maxTime': maxStart
                    },
                    endConfig = {
                        'timeFormat': 'H:i',
                        'step': step,
                        'disableTextInput': true,
                        'minTime': minEnd,
                        'maxTime': '24:00',
                        'show2400': true
                    };

                scope.$watch('model', function (val) {


                    if (!val) {
                        return;
                    }

                    if (theScope) {
                        theScope.$destroy();
                        $el.empty();
                        theScope = null;

                    }

                    theScope = scope.$new(false);


                    var bh = $el.businessHours({

                        operationTime: val,

                        postInit: function () {


                            var start = $('.operationTimeFrom'),
                                end = $('.operationTimeTill');


                            start.timepicker(startConfig);

                            end.timepicker(endConfig);

                            end.each(function (i) {

                                var startImp = $(start[i]),
                                    startTime = startImp.timepicker('getTime'),
                                    minTime = new Date(startTime.getTime() + delta);

                                startImp.data('dayIndex', i);

                                if (maxStartHours === startTime.getHours() && maxStartMinutes === startTime.getMinutes()) {
                                    minTime = '24:00';
                                }


                                $(this).timepicker('option', 'minTime', minTime);

                            });


                            start.on('changeTime', function (e) {

                                var el = $(e.currentTarget),
                                    startTime = el.timepicker('getTime'),
                                    endInput = $(end[el.data('dayIndex')]),
                                    endTime = endInput.timepicker('getTime'),
                                    diff = endTime - startTime,
                                    minTime = new Date(startTime.getTime() + delta);


                                if (maxStartHours === startTime.getHours() && maxStartMinutes === startTime.getMinutes()) {
                                    minTime = '24:00';
                                }


                                endInput.timepicker('option', 'minTime', minTime);


                                if (minTime === '24:00') {
                                    endInput.val('24:00');
                                }


                                if (diff <= 0 && minTime !== '24:00') {
                                    endInput.timepicker('setTime', new Date(minTime));

                                }

                                scope.$apply(function () {
                                    theScope.update();

                                });

                            });


                            end.on('changeTime', function () {
                                scope.$apply(function () {
                                    theScope.update();
                                });
                            });


                            $compile($el.contents())(theScope);


                        },
                        dayTmpl: '<div class="dayContainer" style="width: 80px;">' +
                        '<div data-original-title="" class="colorBox" ng-click="update()"><input type="checkbox" class="ibusinessHoursisible operationState"></div>' +
                        '<div class="weekday"></div>' +
                        '<div class="operationDayTimeContainer">' +
                        '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime" class="mini-time form-control operationTimeFrom" value=""></div>' +
                        '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime" class="mini-time form-control operationTimeTill" value=""></div>' +
                        '</div></div>'
                    });

                    theScope.update = function () {


                        angular.merge(scope.model, bh.serialize());
                        scope.onChange();


                    };


                });


            }

        };

    })
    .directive('customProgress', function () {

        return {

            scope: {
                show: '='
            },

            template: '<div class="load_circle_wrapper"><div class="loading_spinner"><div id="wrap_spinner"><md-progress-circular class="md-primary" md-mode="indeterminate"></md-progress-circular></div></div></div>',


            link: function (scope) {
                scope.$watch('show', function (nv, ov) {

                    if (!nv) {
                        $('.page-loading-overlay').addClass("loaded");
                        $('.load_circle_wrapper').addClass("loaded");

                    } else {
                        $('.page-loading-overlay').removeClass("loaded");
                        $('.load_circle_wrapper').removeClass("loaded");

                    }


                });
            }
        };

    });