angular.module('sd.place', ['ui.router', 'sdDashboard', 'ngMessages', 'angularFileUpload', 'vAccordion', 'ngMaterial', 'uiGmapgoogle-maps', 'custom.ui', 'constants', 'utils'])
    .config(function ($stateProvider, uiGmapGoogleMapApiProvider) {
        $stateProvider
            .state('place', {
                url: '/place',
                views: {
                    'main': {
                        templateUrl: 'place/place.tpl.html'

                    }
                },
                controller: 'PlaceCtrl',
                data: {
                    pageTitle: 'Точки выдачи'
                }
            });

        uiGmapGoogleMapApiProvider.configure({
            // key: 'AIzaSyBT7FYBzWmz0k0IyLCuqFUPymC8Bnep6eQ',
            libraries: 'places' // Required for SearchBox.
        });


    })
    .controller('PlaceCtrl', function ($scope, Network, Place, Notify, Utils, config) {

        $scope.map = {
            ready: false,
            center: {
                latitude: 59.94458145670048,
                longitude: 30.295510590076447
            },
            zoom: 10,
            markers: []

        };

        $scope.selectedLocation = {};


        $scope.config = {
            hourDelta: config.businessHours.hourDelta,
            step: config.businessHours.step
        };


        var selectedIndex = null;

        var minZoomLevel = 5;


        Notify.subscribe('MarkerStartDrag', $scope, function (evt, marker) {
            selectedIndex = marker.key;
            $scope.locations[selectedIndex].selected = true;
        });


        Notify.subscribe('MarkerStopDrag', $scope, function (evt, marker) {
            console.log(marker);
            console.log($scope.locations);


            delete $scope.locations[selectedIndex]['selected'];
        });


        var events = {
            places_changed: function (searchBox) {

                var place = searchBox.getPlaces(),
                    location = {
                        name: '',
                        title: {
                            ru: '',
                            en: ''
                        },
                        lat: '',
                        lng: ''
                    };

                if (!place || place.length === 0) {
                    console.log('no place data :(');
                    return;
                }

                console.dir(place);

                $scope.map.center = {
                    latitude: place[0].geometry.location.lat(),
                    longitude: place[0].geometry.location.lng()
                };


                location.lat = place[0].geometry.location.lat();
                location.lng = place[0].geometry.location.lng();


                var marker = Place.marker(location);


                location.lat = marker.coords.latitude;
                location.lng = marker.coords.longitude;


                Network.post('location', location, true).then(function (res) {

                    console.dir(res);

                    Place.locations[res.id] = res;
                    marker.id = res.id;
                    $scope.selectedLocation = Place.locations[res.id];
                    //marker.model.options.labelContent = res.data.name;


                    $scope.map.markers.push(marker);

                });


            },
            zoom_changed: function () {

            }
        };

        $scope.search = {
            template: 'place/search.tpl.html',
            events: events
        };


        $scope.onNameChange = function (location) {
            if (location.name.length > 0) {
                return Network.put('location', location, true);
            }

        };


        Place.getLocations()
            .then(function (locations) {

                $scope.locations = Place.locations;

                var mapCenter = {};


                // User hasn't locations so far
                if (Object.keys(Place.locations).length === 0) {

                    // Try to determine user location
                    Place.getUserLocation()
                        .then(function (position) {
                            $scope.map.center = {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude
                            };
                            $scope.map.ready = true;


                        }, function () {
                            // User denied Geolocation access, show defaults
                            $scope.map.ready = true;
                        });

                } else {

                    // Get default one

                    mapCenter = _.find(locations, {isDefault: true}) || Place.locations[Object.keys(Place.locations)[0]];

                    $scope.selectedLocation = mapCenter;


                    Place.prepareMarkers(Place.locations)
                        .then(function (res) {
                            $scope.map.ready = true;
                            $scope.map.markers = res;

                        });


                }
            });

        $scope.cbClick = function (evt, loc) {


            Utils.handleDefaults(Place.locations, loc, 'location');

        };


        $scope.onSelect = function (evt, location) {
            evt.stopPropagation();
            $scope.selectedLocation = location;


        };

        $scope.$watch('selectedLocation', function (nV, oV) {
            console.log(nV);
            if (nV && nV.hasOwnProperty('id')) {
                $scope.map.center = {
                    latitude: nV.lat,
                    longitude: nV.lng
                };
            }

        });


        $scope.businessHoursChange = function (location) {
            return Network.put('location', location, true);

        };


        $scope.removePlace = function (evt, place) {
            evt.stopImmediatePropagation();
            evt.preventDefault();
            Network.remove('location', {id: place.id})
                .then(function () {


                    var ii = _.findIndex($scope.map.markers, function (o) {
                        return o.id === place.id;
                    });
                    var newDefault = Place.locations[Object.keys(Place.locations)[0]];


                    if (place.isDefault && Object.keys(Place.locations).length > 0) {

                        // Set a new default location

                        Network.put('location', newDefault, true)
                            .then(function (res) {

                                newDefault.isDefault = true;
                                $scope.selectedLocation = newDefault;


                            });

                    }

                    delete Place.locations[place.id];

                    $scope.map.markers.splice(ii, 1);
                });
        };


    })

    .service('Place', function ($q, Network, Notify, Helpers) {
        return {

            locations: {},

            getLocations: function () {
                var that = this;

                return $q(function (resolve, reject) {
                    Network.get('user')
                        .then(function (user) {
                            Network.get('location')
                                .then(function (places) {
                                    that.locations = Helpers.id2Index(places);
                                    console.dir(that.locations);
                                    return resolve(that.locations);
                                });
                        });
                });

            },

            prepareMarkers: function (locations) {
                var markers = [],
                    that = this;

                return $q(function (resolve, reject) {
                    async.forEach(Object.keys(locations), function (location, cb) {

                        var marker = that.marker(locations[location]);
                        markers.push(marker);
                        cb();
                    }, function () {
                        return resolve(markers);
                    });
                });

            },

            marker: function (location) {
                var that = this,
                    marker = {
                        id: location.id || 0,

                        coords: {
                            latitude: location.lat,
                            longitude: location.lng
                        },
                        options: {
                            draggable: true,
                            icon: {
                                url: '//static.selectdrive.ru/assets/images/marker.png'
                            },
                            labelClass: 'maplabels',
                            labelAnchor: '25 0',
                            labelVisible: true,
                            labelContent: location.name

                        },
                        events: {
                            dragstart: function (marker, eventName, args) {
                                Notify.emit('MarkerStartDrag', marker);

                            },
                            dragend: function (marker, eventName, args) {
                                console.dir(marker);

                                var updated = {
                                    lat: marker.position.lat(),
                                    lng: marker.position.lng(),
                                    id: marker.key
                                };


                                Network.put('location', updated, true)
                                    .then(function (result) {

                                        if (result.name && result.name.length > 0) {
                                            marker.model.options.labelContent = result.name;
                                        }
                                        that.locations[result.id] = result;
                                        Notify.emit('MarkerStopDrag', marker);

                                    })
                                    .catch(function (err) {

                                        return alert("Ошибка: " + err.data.error);
                                    });


                            }

                        }
                    };

                return marker;
            },


            getUserLocation: function () {
                return $q(function (resolve, reject) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        return resolve(position);
                    }, function () {
                        return reject(false);
                    });
                });

            }


        };
    })
    .filter('placeTitle', function () {
        return function (place) {

            if (!place.title) {
                return '';
            }

            if (place.name) {
                return place.name;
            }
            return place.title.ru;
        };

    });
